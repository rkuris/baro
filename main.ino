#include <Entropy.h>

void setup() {
  Entropy.Initialize();
  randomSeed(Entropy.random());
  Serial.begin(115200);

  events::setup();
  lcd::setup();
  canbus::setup();
  buttons::setup();
  clock::setup();
  baro::setup();
}

void loop() {
  delay(500);
}
