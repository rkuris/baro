#include <TimeLib.h>

namespace clock {
time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}
void setup() {
  setSyncProvider(getTeensy3Time);
}

char nowstr_[16];
char *nowstr() {
  sprintf(nowstr_, "%02d:%02d:%02d", hour(), minute(), second());
  return nowstr_;
}
}
