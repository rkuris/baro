#include <U8g2lib.h>
#include <U8x8lib.h>
#include <EventResponder.h>

namespace lcd {
enum {
  PIN_SCL = 13,
  PIN_SDA = 11,
  PIN_RES = 8,
  PIN_DC = 9,
  PIN_CS = 10
};
U8G2_SSD1309_128X64_NONAME0_F_4W_HW_SPI lcd(U8G2_R0, PIN_CS, PIN_DC, PIN_RES);
EventResponder heartbeatevent;
MillisTimer heartbeattimer;

const auto XSIZE = 128;
const auto YSIZE = 64;
const char *dice[6] = {
  "\xe2\x9a\x80",
  "\xe2\x9a\x81",
  "\xe2\x9a\x82",
  "\xe2\x9a\x83",
  "\xe2\x9a\x84",
  "\xe2\x9a\x85"
};

int dieSelector() {
  static int last_hour = -1;
  static int current_die;
  if (last_hour != hour()) {
    current_die = random(0,6);
    last_hour = hour();
  }
  return current_die;
}
void updateLCD(EventResponderRef event)
{
  lcd.firstPage();
  do {
    lcd.setFont(u8g2_font_8x13_t_symbols);
    lcd.drawStr(0, 1 * YSIZE / 4 - 1, ::clock::nowstr());
    lcd.drawStr(0, 2 * YSIZE / 4 - 1, ::baro::tempStr());
    lcd.drawStr(0, 3 * YSIZE / 4 - 1, ::baro::baroStr());
    lcd.drawUTF8(55, YSIZE - 1, dice[dieSelector()]);
    /*lcd.drawLine(XSIZE / 2, 0,         XSIZE / 2, YSIZE - 1);
    lcd.drawLine(XSIZE / 2, 0,         XSIZE - 1, 0);
    lcd.drawLine(XSIZE / 2, YSIZE - 1, XSIZE - 1, YSIZE - 1);
    lcd.drawLine(XSIZE - 1, 0,         XSIZE - 1, YSIZE - 1);*/
  } while (lcd.nextPage());
}
void setup() {
  lcd.begin();
  heartbeatevent.attach(updateLCD);
  heartbeattimer.beginRepeating(1000, heartbeatevent);

  // hook up the event timer loop to systick
  events::addSystickCallback(heartbeattimer.runFromTimer);
}
}
