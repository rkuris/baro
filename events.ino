namespace events {
const int MAXCALLBACKS = 4;
void (*(callbacks[MAXCALLBACKS]))();
volatile int ncallbacks = 0;

void systick_isr() {
  for (auto i = 0; i < ncallbacks; i++) {
    callbacks[i]();
  }
}

void addSystickCallback(void ((*f)())) {
  callbacks[ncallbacks] = f;
  ncallbacks++;
}

void setup() {
  // add system ticker to list, call it first
  addSystickCallback(_VectorsRam[15]);
  // then redirect systicks to ours
  _VectorsRam[15] = systick_isr;
}

}
