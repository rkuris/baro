#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

namespace events {
void addSystickCallback(void ((*)()));
}

namespace baro {

MillisTimer heartbeattimer;
EventResponder heartbeatevent;

Adafruit_BME280 bme;

unsigned long delayTime;

int status = 0;
float temp = NAN;
float humidity = NAN;
float pressure = NAN;

inline float c2f(float c) {
  return 9.0 * c / 5 + 32;
}

void updateBarometer(EventResponderRef event) {
  if (status) {
    temp = c2f(bme.readTemperature());
    humidity = bme.readHumidity();
    pressure = bme.readPressure();
  }
}

const char *tempStr() {
  static char t[3/*100*/ + 1/*o*/ + 1/* */ + 3/*100*/ + 1/*%*/ + 1/*null*/ + 1/*spare*/];
  if (isnan(temp)) {
    return "?";
  }
  sprintf(t, "%.0f\xb0 %.0f%%", temp, humidity);
  return t;
}

inline float pascals_to_inhg(float p) {
  const float PASCALS_TO_INHG = 3386.3886666667;

  return p / PASCALS_TO_INHG;
}
const char *baroStr() {
  if (isnan(pressure)) {
    return "?";
  }
  static char b[5/*29.95*/ + 1/*"*/ + 1/*null*/ + 1/*extra*/];
  sprintf(b, "%2.2f\"", pascals_to_inhg(pressure));
  return b;
}

void setup() {
  const int I2CPORT = 0x76;
  const int CHECK_INTERVAL_SECONDS = 2500;
  status = bme.begin(I2CPORT);
  heartbeatevent.attach(updateBarometer);
  heartbeattimer.beginRepeating(CHECK_INTERVAL_SECONDS, heartbeatevent);

  // hook up the event timer loop to systick
  events::addSystickCallback(heartbeattimer.runFromTimer);
}
}
