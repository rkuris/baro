#include <FlexCAN_T4.h>
#include <EventResponder.h>

namespace canbus {
FlexCAN_T4<CAN1, RX_SIZE_256, TX_SIZE_16> Can;
EventResponder canevent;

void CANisr(const CAN_message_t &msg) {
  canevent.triggerEvent(0, &const_cast<CAN_message_t&>(msg));
}

void handleCANEvent(EventResponderRef event)
{
  CAN_message_t *msg = (CAN_message_t *)event.getData();
}
void setup() {
  const auto CANBAUD = 250000;
  const auto MAXMB = 16;
  Can.begin();
  Can.setBaudRate(CANBAUD);
  Can.setMaxMB(MAXMB);
  Can.enableFIFO();
  Can.enableFIFOInterrupt();
  Can.onReceive(CANisr);
  canevent.attach(handleCANEvent);
}

}
